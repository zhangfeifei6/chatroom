<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<% 
  //中文处理
  request.setCharacterEncoding("UTF-8");

  String name=request.getParameter("name");
   List<String> list=(List<String>)application.getAttribute("users");
   if(list==null){
	   list=new ArrayList();
   }
   list.add(name);
   application.setAttribute("users", list);
   
   //用户的处理
   session.setAttribute("u", name);
   
    
   
   //跳转到main.jsp
   response.sendRedirect("main.jsp");
  
%>
</body>
</html>